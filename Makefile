# Ant Makefile

all:
	ant debug

release:
	ant release

install: all
	adb -d install -r bin/ClearOS_MobileDemo-debug.apk

run:
	adb -d shell am start -a android.intent.action.MAIN -n com.clearcenter.mobile_demo/.mdMainActivity

logcat:
	adb -d logcat

clean:
	ant clean

key:
	keytool -genkey -v -keystore ~/.android/release.keystore -alias clear -keyalg RSA -keysize 2048 -validity 10000
