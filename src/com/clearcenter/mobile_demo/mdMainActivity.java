// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import com.clearcenter.mobile_demo.mdStatusActivity;
import com.clearcenter.mobile_demo.mdAuthenticatorActivity;
import com.clearcenter.mobile_demo.db.mdDeviceSample.mdDeviceSamples;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.GeneralSecurityException;
import java.util.Date;
import java.text.DateFormat;

import org.json.JSONException;
import org.json.JSONObject;

public class mdMainActivity extends Activity
{
    private static final String TAG = "mdMainActivity";

    private TextView accounts_textview;
    private ListView accounts_listview;
    private ArrayAdapter<String> accounts_adapter = null;

    private AccountManager account_manager;

    public String account_to_delete = null;

    private class mdAccountManagerCallback implements AccountManagerCallback<Boolean> {
        public void run(AccountManagerFuture<Boolean> result) {
            Log.i(TAG, "Remove account result: " + result);
            mdMainActivity.this.refreshAccounts();
        }
    };

    // Create main/default activity
    public void onCreate(Bundle bundle)
    {
        Log.i(TAG, "onCreate(" + bundle + ")");
        super.onCreate(bundle);

        setContentView(R.layout.main_activity);

        Log.i(TAG, "loading data from Intent");
        final Intent intent = getIntent();
        Log.i(TAG, "onCreate intent: " + intent);
        final Bundle extras = getIntent().getExtras();
        Log.i(TAG, "onCreate intent extras: " + extras);

        accounts_textview = (TextView) findViewById(R.id.accounts_textview);
        accounts_listview = (ListView) findViewById(R.id.accounts_listview);
        accounts_adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1, android.R.id.text1);
        accounts_listview.setAdapter(accounts_adapter);
        accounts_listview.setOnItemClickListener(
            new OnItemClickListener() {
            public void onItemClick(
                AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 0) {
                    final Intent intent = new Intent(
                        mdMainActivity.this, mdAuthenticatorActivity.class);
                    mdMainActivity.this.startActivity(intent);
                        //new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT), 0);
                }
                else {
                    final Intent intent = new Intent(
                        mdMainActivity.this, mdStatusActivity.class);
                    intent.putExtra("nickname",
                        mdMainActivity.this.accounts_adapter.getItem(position));
                    mdMainActivity.this.startActivity(intent);
                }
            }
        });

        accounts_listview.setOnItemLongClickListener(
            new OnItemLongClickListener() {
            public boolean onItemLongClick(
                AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 0) return false;
                final String nickname = mdMainActivity.this.accounts_adapter.getItem(position);
                mdMainActivity.this.account_to_delete = nickname;
                AlertDialog alert_dialog = new AlertDialog.Builder(mdMainActivity.this).create();
                //alert_dialog.setIcon(R.drawable.ic_launcher);
                alert_dialog.setTitle("Remove System?");
                alert_dialog.setMessage("Are you sure you want to remove the " + nickname + " system account?");
                alert_dialog.setButton(
                    DialogInterface.BUTTON_POSITIVE, "Remove system",
                    new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which) {
                        mdMainActivity.this.onRemoveAccount();
                    }
                });
                alert_dialog.setButton(
                    DialogInterface.BUTTON_NEGATIVE, "Cancel",
                    new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which) { }
                });

                alert_dialog.show();
                return true;
            }
        });

        account_manager = AccountManager.get(this);
    }

    protected void onResume()
    {
        super.onResume();

        refreshAccounts();
    }

    public void onRemoveAccount()
    {
        Log.i(TAG, "Remove account: " + account_to_delete);
        final Account account = new Account(
            account_to_delete, mdConstants.ACCOUNT_TYPE);
        account_manager.removeAccount(account,
            new mdAccountManagerCallback(), null);
    }

    public void refreshAccounts()
    {
        final Account accounts[] = account_manager.getAccountsByType(
            mdConstants.ACCOUNT_TYPE);
        accounts_adapter.clear();
        accounts_adapter.add(
            getText(R.string.main_activity_add_account).toString());
        for (Account account : accounts)
            accounts_adapter.add(account.name);
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
