// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import com.clearcenter.mobile_demo.db.mdDeviceSample.mdDeviceSamples;
import com.clearcenter.mobile_demo.providers.mdContentProvider;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SyncInfo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.LinkedList;

import org.json.JSONException;
import org.json.JSONObject;

public class mdStatusActivity extends Activity
{
    private static final String TAG = "mdStatusActivity";

    private mdDeviceSamplesObserver observer = null;

    private Handler observer_handler = new Handler();

    private TextView hostname_textview;
    private TextView release_textview;
    private TextView time_textview;

    private int samples = 40;

    private mdGraphView bw_graphview;
    private int bw_graph_up = -1;
    private int bw_graph_down = -1;

    private mdGraphView loadavg_graphview;
    private int loadavg_graph_5min = -1;
    private int loadavg_graph_15min = -1;

    private mdGraphView mem_graphview;
    private int mem_graph_active = -1;
    private int mem_graph_swap = -1;

    //private AccountManager account_manager;
    private String account_nickname = null;

    class mdDeviceSamplesObserver extends ContentObserver
    {
        private mdStatusActivity activity;

        public mdDeviceSamplesObserver(Handler handler, mdStatusActivity activity)
        {
            super(handler);
            this.activity = activity;
        }

        public void onChange(boolean selfChange)
        {
            Log.d(TAG, "onChange(" + selfChange + ")");
            activity.updateData();
        }
    }

    // Create system status activity
    public void onCreate(Bundle bundle)
    {
        Log.i(TAG, "onCreate(" + bundle + ")");
        super.onCreate(bundle);

        setContentView(R.layout.status_activity);

        //final Intent intent = getIntent();
        //final Bundle extras = getIntent().getExtras();

        account_nickname = getIntent().getStringExtra("nickname");
        setTitle(account_nickname);

        hostname_textview = (TextView) findViewById(R.id.hostname_textview);
        release_textview = (TextView) findViewById(R.id.release_textview);
        time_textview = (TextView) findViewById(R.id.time_textview);

        bw_graphview = (mdGraphView)this.findViewById(R.id.bandwidth_view);
        bw_graph_up = bw_graphview.addItem("Upstream", 0xFF99CC00);
        bw_graph_down = bw_graphview.addItem("Downstream", 0xFF33B5E5);
        bw_graphview.reset();

        loadavg_graphview = (mdGraphView)this.findViewById(R.id.loadavg_view);
        loadavg_graph_5min = loadavg_graphview.addItem("5 Minute", 0xFF99CC00);
        loadavg_graph_15min = loadavg_graphview.addItem("15 Minute", 0xFF33B5E5);
        loadavg_graphview.reset();

        mem_graphview = (mdGraphView)this.findViewById(R.id.memory_view);
        mem_graph_active = mem_graphview.addItem("Active", 0xFF99CC00);
        mem_graph_swap = mem_graphview.addItem("Swapped", 0xFF33B5E5);
        mem_graphview.reset();

        //account_manager = AccountManager.get(this);
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();

        if (android.os.Build.VERSION.SDK_INT >= 11)
            inflater.inflate(R.menu.status_activity, menu);
        else 
            inflater.inflate(R.menu.status_activity_v7, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        ContentResolver resolver = getContentResolver();
        final Account account = new Account(
            account_nickname, mdConstants.ACCOUNT_TYPE);

        switch (item.getItemId()) {
        case R.id.menu_refresh:
            resolver.requestSync(account,
                mdContentProvider.AUTHORITY, new Bundle());
            return true;
        case R.id.menu_reset:
            resetData();
            return true;
        }

        return false;
    }

    protected void onResume()
    {
        super.onResume();

        observer = new mdDeviceSamplesObserver(observer_handler, this);
        ContentResolver resolver = getContentResolver();
        resolver.registerContentObserver(
            mdDeviceSamples.CONTENT_URI, true, observer);

        updateData();

        final Account account = new Account(
            account_nickname, mdConstants.ACCOUNT_TYPE);
        resolver.requestSync(account,
            mdContentProvider.AUTHORITY, new Bundle());

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            List<SyncInfo> syncs = resolver.getCurrentSyncs();
            for (SyncInfo info : syncs) {
                Log.d(TAG, "account: " + info.account.name +
                    ", authority: " + info.authority + ", startTime: " +
                    info.startTime);
            }
        }
    }

    protected void onPause()
    {
        super.onPause();

        ContentResolver resolver = getContentResolver();
        resolver.unregisterContentObserver(observer);
    }

    public void updateData()
    {
        JSONObject json_data;

        String projection[] = new String[] {
            mdDeviceSamples.DATA
        };

        Cursor cursor = getContentResolver().query(
            mdDeviceSamples.CONTENT_URI,
            projection,
            mdDeviceSamples.NICKNAME + " = ?",
            new String[] { account_nickname }, null);

        int rows = 0;
        try {
            rows = cursor.getCount();
        } catch (NullPointerException e) { }

        Log.d(TAG, "Matches: " + rows);

        if (rows == 0) {
            Log.d(TAG, "No rows match for nickname: " + account_nickname);
            cursor.close();
            return;
        }

        cursor.moveToLast();

        String data = cursor.getString(
            cursor.getColumnIndex(mdDeviceSamples.DATA)); 

        try {
            json_data = new JSONObject(data);
            if (json_data.has("hostname"))
                hostname_textview.setText(
                    "Hostname: " + json_data.getString("hostname"));
            if (json_data.has("name") && json_data.has("release")) {
                final String release = json_data.getString("name") + " " +
                    json_data.getString("release");
                release_textview.setText("Release: " + release);
            }
            if (json_data.has("time_locale")) {
                time_textview.setText("Clock: " +
                    json_data.getString("time_locale"));
            }

            if (rows >= 2) {
                bw_graphview.reset();
                loadavg_graphview.reset();
                mem_graphview.reset();

                if (rows <= samples)
                    cursor.moveToFirst();
                else
                    cursor.move(-samples);

                long unix_time = 0;
                double bw_up = 0.0;
                double bw_down = 0.0;

                do {
                    data = cursor.getString(
                        cursor.getColumnIndex(mdDeviceSamples.DATA)); 

                    final List<JSONObject> samples = sortedSamplesList(data);
                    ListIterator<JSONObject> li = samples.listIterator();

                    while (li.hasNext()) {
                        json_data = li.next();

                        if (unix_time == 0) {
                            bw_up = json_data.getDouble("bandwidth_up");
                            bw_down = json_data.getDouble("bandwidth_down");
                            unix_time = Long.valueOf(json_data.getString("time"));
                            continue;
                        }

                        long diff = Long.valueOf(json_data.getString("time")) -
                            unix_time;

                        double rate_up =
                            (json_data.getDouble("bandwidth_up") - bw_up) / diff;
                        double rate_down =
                            (json_data.getDouble("bandwidth_down") - bw_down) / diff;

                        if (rate_up < 0.0) rate_up = 0.0;
                        if (rate_down < 0.0) rate_down = 0.0;

                        bw_graphview.addSample(
                            bw_graph_up, unix_time, (float)rate_up);
                        bw_graphview.addSample(
                            bw_graph_down, unix_time, (float)rate_down);

//                        Log.d(TAG, "time: " + diff +
//                            ", rate_up: " + rate_up + ", rate_down: " + rate_down);
                        bw_up = json_data.getDouble("bandwidth_up");
                        bw_down = json_data.getDouble("bandwidth_down");

                        loadavg_graphview.addSample(
                            loadavg_graph_5min, unix_time,
                            (float)json_data.getDouble("loadavg_5min"));
                        loadavg_graphview.addSample(
                            loadavg_graph_15min, unix_time,
                            (float)json_data.getDouble("loadavg_15min"));

                        mem_graphview.addSample(
                            mem_graph_active, unix_time,
                            (float)json_data.getDouble("mem_active"));
                        mem_graphview.addSample(
                            mem_graph_swap, unix_time,
                            (float)json_data.getDouble("mem_swap_used"));

                        unix_time = Long.valueOf(json_data.getString("time"));
                    }
                } while (cursor.moveToNext());

                bw_graphview.update();
                loadavg_graphview.update();
                mem_graphview.update();
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSONException", e);
        }

        cursor.close();
    }

    public void resetData()
    {
        int count = getContentResolver().delete(
            mdDeviceSamples.CONTENT_URI,
            mdDeviceSamples.NICKNAME + " = ?",
            new String[] { account_nickname });
        Log.d(TAG, "Reset database, purged " + count + " rows.");

        bw_graphview.reset();
        bw_graphview.update();
        loadavg_graphview.reset();
        loadavg_graphview.update();
        mem_graphview.reset();
        mem_graphview.update();

        ContentResolver resolver = getContentResolver();
        final Account account = new Account(
            account_nickname, mdConstants.ACCOUNT_TYPE);
        resolver.requestSync(account,
            mdContentProvider.AUTHORITY, new Bundle());
    }

    public List<JSONObject> sortedSamplesList(String data)
    {
        SortedMap<String, JSONObject> map = new TreeMap<String, JSONObject>();

        try {
            int version = 0;
            JSONObject json_data = new JSONObject(data);
            if (json_data.has("version"))
                version = json_data.getInt("version");
            if (version < 1 && json_data.has("time")) {
                String key = json_data.getString("time");
                map.put(key, json_data);
            }
            else if (version >= 1 &&
                    json_data.has("samples") &&
                    !json_data.isNull("samples")) {
                json_data = json_data.getJSONObject("samples");
                Iterator i = json_data.keys();
                while (i.hasNext()) {
                    String key = i.next().toString();
                    JSONObject sample = json_data.getJSONObject(key);
                    sample.put("time", key);
                    map.put(key, sample);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSONException", e);
        }

        return new LinkedList<JSONObject>(map.values());
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
