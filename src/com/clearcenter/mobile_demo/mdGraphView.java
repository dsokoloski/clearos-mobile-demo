// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.ListIterator;
import java.util.Vector;

class mdGraphItem extends Object
{
    public String name;
    public String legend_text;
    public int color;
    public float threshold;
    public float max_value;
    public float avg_value;
    public Vector<mdGraphSample> sample;
}

class mdGraphSample extends Object
{
    public long interval;
    public float value;
}

public class mdGraphView extends View
{
    private final String TAG = "mdGraphView";

    private Bitmap bitmap = null;
    private Bitmap bitmap_legend = null;
    private Boolean display_legend = false;
    private Paint paint = new Paint();
    private Canvas canvas = new Canvas();
    private Vector<mdGraphItem> item = new Vector<mdGraphItem>();
    private float max_value = 0.0f;
    private long min_interval = 0;
    private long max_interval = 0;
    private int grid_size = 8;
    
    public mdGraphView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setOnClickListener(
            new OnClickListener() {
            public void onClick(View v) { mdGraphView.this.onClick(v); }
            }
        );

        invalidate();
    }

    public mdGraphView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        invalidate();
    }

    public void onClick(View v)
    {
        display_legend = (display_legend) ? false : true;
        render();
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(TAG, "onSizeChanged: w: " + w + ", h: " + h);
        render();
    }
    
    public int addItem(String name, int color)
    {
        mdGraphItem item = new mdGraphItem();
        item.name = name;
        item.color = color;
        item.max_value = 0.0f;
        item.avg_value = 0.0f;
        return addItem(item);
    }
    
    private int addItem(mdGraphItem item)
    {
        item.threshold = 0.0f;
        item.sample = new Vector<mdGraphSample>();
        this.item.add(item);
        return this.item.indexOf(item);
    }
    
    public void setItemThreshold(int item, float threshold)
    {
        mdGraphItem i = this.item.get(item);
        if (i == null) return;
        i.threshold = threshold;
    }
    
    public void addSample(int item, long interval, float value)
    {
        if (value == Float.POSITIVE_INFINITY) {
            Log.d(TAG, "Ignoring infinity value!");
            return;
        }

        mdGraphItem i = this.item.get(item);
        if (i == null) return;
        mdGraphSample s = new mdGraphSample();
        s.interval = interval;
        s.value = value;
        if (min_interval == 0) min_interval = interval;
        else if (interval < min_interval) min_interval = interval;
        i.sample.add(s);
    }
    
    public void reset()
    {
        bitmap = null;
        bitmap_legend = null;
        min_interval = 0;
        max_interval = 0;
        max_value = 0.0f;
        mdGraphItem i;
        
        ListIterator<mdGraphItem> li = item.listIterator();
        while (li.hasNext()) {
            i = li.next();          
            i.sample.clear();
        }
    }
    
    public void update()
    {
        max_value = 0.0f;

        float avg_total = 0.0f;

        mdGraphItem i = null;
        mdGraphSample s = null;
        ListIterator<mdGraphSample> si;
        ListIterator<mdGraphItem> li = item.listIterator();

        while (li.hasNext()) {
            avg_total = 0.0f;
            i = li.next();
            i.max_value = 0.0f;
            si = i.sample.listIterator();

            while (si.hasNext()) {
                s = si.next();
                if (s.value > max_value) max_value = s.value;
                if (s.value > i.max_value) i.max_value = s.value;
                if (s.interval > max_interval) max_interval = s.interval;
                avg_total += s.value;
            }

            i.avg_value = avg_total / (float)i.sample.size();
        }
        
        render();
    }

    private float getMaxVisibleValue(int interval, int width)
    {
        float x;
        float max_visible_value = 0.0f;

        mdGraphItem i = null;
        mdGraphSample s = null;
        ListIterator<mdGraphSample> si;
        ListIterator<mdGraphItem> li = item.listIterator();
        while (li.hasNext()) {
            i = li.next();
            si = i.sample.listIterator();
            while (si.hasNext()) {
                s = si.next();
                if (interval <= width)
                    x = (float)(s.interval - min_interval);
                else
                    x = (float)(width - interval) + (float)(s.interval - min_interval);
                if (x >= 0 && x <= width && s.value > max_visible_value)
                    max_visible_value = s.value;
            }
        }

        return (max_visible_value < 1.0f) ? 1.0f : max_visible_value;
    }

    public void onDraw(Canvas canvas)
    {
        Log.d(TAG, "onDraw");
        
        synchronized (this) {
            final int w = getWidth();
            final int h = getHeight();
            
            long m = System.currentTimeMillis();
            
            canvas.drawColor(0x00000000);
            
            paint.reset();
            paint.setColor(0xFF303030);
            
            for (int x = w - grid_size - 1; x > 0; x -= grid_size)
                canvas.drawLine(x, 0, x, h, paint);
            for (int y = h - grid_size - 1; y > 0; y -= grid_size)
                canvas.drawLine(0, y, w, y, paint);
            
            mdGraphItem i;
            ListIterator<mdGraphItem> li = item.listIterator();
            while (li.hasNext()) {
                i = li.next();
                if (i.threshold == 0.0f) continue;
                paint.setColor(i.color);
                paint.setAlpha(0xa0);
                float y = i.threshold * (float)h / max_value;
                canvas.drawLine(0, (float)h - y, w, (float)h - y, paint);
            }
            
            if (bitmap != null) {
                int offset = 0;
                if (bitmap.getWidth() > w) offset = bitmap.getWidth() - w;
                Rect src = new Rect();
                src.top = 0;
                src.bottom = h;
                src.left = offset;
                src.right = offset + w;
                Rect dst = new Rect();
                dst.top = 0;
                dst.bottom = h;
                dst.left = 0;
                dst.right = (bitmap.getWidth() > w ? w : bitmap.getWidth());

                canvas.drawBitmap(bitmap, src, dst, null);
            }

            if (bitmap_legend != null && display_legend) {
                Rect src = new Rect();
                src.top = 0;
                src.bottom = bitmap_legend.getHeight();
                src.left = 0;
                src.right = bitmap_legend.getWidth();
                Rect dst = new Rect();
                dst.top = (h - bitmap_legend.getHeight()) / 2;
                dst.bottom = dst.top + bitmap_legend.getHeight();
                dst.left = (w - bitmap_legend.getWidth()) / 2;
                dst.right = dst.left + bitmap_legend.getWidth();

                canvas.drawBitmap(bitmap_legend, src, dst, null);
            }

            Log.d(TAG, "draw time: " +
                (System.currentTimeMillis() - m) + "ms");
        }
    }

    private void render()
    {
        final int w = getWidth();
        final int h = getHeight();

        if (w == 0 || h == 0) return;

        long m = System.currentTimeMillis();

        render_graph(w, h);
        if (display_legend && item.size() > 0)
            render_legend(w, h);

        Log.d(TAG, "render time: " +
            (System.currentTimeMillis() - m) + "ms");

        invalidate();
    }

    private void render_graph(int w, int h)
    {
        bitmap = null;
        int interval = (int)(max_interval - min_interval);

        Log.d(TAG,
            "max_interval: " + max_interval +
            ", min_interval: " + min_interval +
            ", interval: " + interval);

        if (interval <= 0) return;

        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        canvas.setBitmap(bitmap);
        canvas.drawARGB(0, 0, 0, 0);

        paint.reset();
        paint.setAntiAlias(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(2.0f);

        mdGraphItem i = null;
        mdGraphSample s = null;
        ListIterator<mdGraphSample> si;
        ListIterator<mdGraphItem> li = item.listIterator();
        float lx, ly, cx, cy;
        float max_visible_value = getMaxVisibleValue(interval, w);

        while (li.hasNext()) {
            i = li.next();
            if (i.sample.size() == 0) continue;

            paint.setColor(i.color);

            si = i.sample.listIterator();

            lx = 0;
            ly = 0;

            while (si.hasNext()) {
                s = si.next();
                if (interval <= w)
                    cx = (float)(s.interval - min_interval);
                else
                    cx = (float)(w - interval) + (float)(s.interval - min_interval);

                cy = s.value * (float)h / max_visible_value;
                canvas.drawLine(lx, (float)h - ly,
                    cx, (float)h - cy, paint);

                lx = cx;
                ly = cy;
            }
        }
    }

    private void render_legend(int w, int h)
    {
        bitmap_legend = null;

        paint.reset();
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        paint.setTextSize(20.0f);

        Rect rect = new Rect();
        Vector<Rect> bounds = new Vector<Rect>();
        mdGraphItem i = null;
        ListIterator<mdGraphItem> li = item.listIterator();

        while (li.hasNext()) {
            i = li.next();
            i.legend_text = String.format(
                "%s %.02f max %.02f avg",
                i.name, i.max_value, i.avg_value);
            Rect r = new Rect();
            paint.getTextBounds(
                i.legend_text,
                0, i.legend_text.length(), r);
            if (r.width() > rect.right)
                rect.right = r.right;
            rect.bottom += r.height();
            bounds.add(r);
        }

        float offset_x = 6.0f;
        float offset_y = 6.0f;
        float line_space = 3.0f;

        rect.right += (int)(offset_x * 2.0f);
        rect.bottom += (int)(offset_y * 2.0f);
        rect.bottom += (int)(line_space * (bounds.size() - 1));

        bitmap_legend = Bitmap.createBitmap(
            rect.right, rect.bottom, Bitmap.Config.ARGB_8888);

        canvas.setBitmap(bitmap_legend);
        paint.setColor(0xb0000000);
        paint.setStrokeWidth(2.0f);
        canvas.drawRoundRect(
            new RectF(0, 0, rect.right, rect.bottom), 6, 6, paint);

        float y = offset_y;
        li = item.listIterator();
        ListIterator<Rect> bi = bounds.listIterator();

        while (li.hasNext()) {
            i = li.next();
            Rect r = bi.next();

            paint.setColor(i.color);

            canvas.drawText(i.legend_text,
                (rect.right - r.width()) / 2.0f,
                y + r.height() - r.bottom, paint);

            y += r.height() + line_space;
        }
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
