// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class mdAuthenticationService extends Service
{
    private static final String TAG = "mdAuthenticationService";

    private mdAuthenticator authenticator;

    public void onCreate()
    {
        if (Log.isLoggable(TAG, Log.VERBOSE))
            Log.v(TAG, "started.");
        authenticator = new mdAuthenticator(this);
    }

    public void onDestroy()
    {
        if (Log.isLoggable(TAG, Log.VERBOSE))
            Log.v(TAG, "stopped.");
    }

    public IBinder onBind(Intent intent)
    {
        if (Log.isLoggable(TAG, Log.VERBOSE))
            Log.v(TAG, "returning the binder for intent " + intent);
        return authenticator.getIBinder();
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
