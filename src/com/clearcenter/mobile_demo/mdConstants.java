// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

public class mdConstants
{
    public static final int MAX_SAMPLES = 100;

    // Account type string.
    public static final String ACCOUNT_TYPE = "com.clearcenter.mobile_demo";

    // Authtoken type string.
    public static final String AUTHTOKEN_TYPE = "com.clearcenter.mobile_demo";
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
