// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import com.clearcenter.mobile_demo.mdRest;
import com.clearcenter.mobile_demo.mdSSLUtil;
import com.clearcenter.mobile_demo.mdConstants;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;

import java.security.GeneralSecurityException;

class mdAuthenticator extends AbstractAccountAuthenticator
{
    // The tag used to log to adb console.
    private static final String TAG = "mdAuthenticator";

    // Authentication Service context
    private final Context ctx;

    public mdAuthenticator(Context context)
    {
        super(context);
        ctx = context;
    }

    public Bundle addAccount(
        AccountAuthenticatorResponse response, String accountType,
        String authTokenType, String[] requiredFeatures, Bundle options)
    {
        Log.v(TAG, "addAccount()");
        final Intent intent = new Intent(ctx, mdAuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    public Bundle confirmCredentials(AccountAuthenticatorResponse response,
        Account account, Bundle options)
    {
        Log.v(TAG, "confirmCredentials()");
        //return null;
        final Intent intent = new Intent(ctx, mdAuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    public Bundle editProperties(AccountAuthenticatorResponse response,
        String accountType)
    {
        Log.v(TAG, "editProperties()");
        throw new UnsupportedOperationException();
    }

    public Bundle getAuthToken(AccountAuthenticatorResponse response,
        Account account, String authTokenType, Bundle loginOptions)
        throws NetworkErrorException
    {
        Log.v(TAG, "getAuthToken()");

        // If the caller requested an authToken type we don't support, then
        // return an error
        if (!authTokenType.equals(mdConstants.AUTHTOKEN_TYPE)) {
            final Bundle result = new Bundle();
            result.putString(
                AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
            return result;
        }

        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        final AccountManager am = AccountManager.get(ctx);
        final String password = am.getPassword(account);
        final String hostname = am.getUserData(account, "hostname");
        final String username = am.getUserData(account, "username");

        if (password != null) {
            try {
                mdSSLUtil.DisableSecurity();

                final String authToken = mdRest.Login(
                    hostname, username, null, password);
                if (!TextUtils.isEmpty(authToken)) {
                    final Bundle result = new Bundle();
                    result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                    result.putString(AccountManager.KEY_ACCOUNT_TYPE, mdConstants.ACCOUNT_TYPE);
                    result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                    return result;
                }
            } catch (JSONException e) {
                Log.e(TAG, "JSONException", e);
            } catch (GeneralSecurityException e) {
                Log.e(TAG, "GeneralSecurityException", e);
            }
        }

        Log.v(TAG, "Asking for password again...");

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our mdAuthenticatorActivity panel.
        final Intent intent = new Intent(ctx, mdAuthenticatorActivity.class);
        intent.putExtra(mdAuthenticatorActivity.PARAM_NICKNAME, account.name);
        intent.putExtra(mdAuthenticatorActivity.PARAM_USERNAME, username);
        intent.putExtra(mdAuthenticatorActivity.PARAM_HOSTNAME, hostname);
        intent.putExtra(mdAuthenticatorActivity.PARAM_AUTHTOKEN_TYPE, authTokenType);
        intent.putExtra(mdAuthenticatorActivity.PARAM_CONFIRM_CREDENTIALS, true);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);

        return bundle;
    }

    public String getAuthTokenLabel(String authTokenType)
    {
        // null means we don't support multiple authToken types
        Log.v(TAG, "getAuthTokenLabel()");
        return null;
    }

    public Bundle hasFeatures(AccountAuthenticatorResponse response,
        Account account, String[] features)
    {
        // This call is used to query whether the Authenticator supports
        // specific features. We don't expect to get called, so we always
        // return false (no) for any queries.
        Log.v(TAG, "hasFeatures()");
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }

    public Bundle updateCredentials(AccountAuthenticatorResponse response,
        Account account, String authTokenType, Bundle loginOptions)
    {
        Log.v(TAG, "updateCredentials()");
        return null;
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
