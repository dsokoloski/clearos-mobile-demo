// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo.providers;

import com.clearcenter.mobile_demo.db.mdDeviceSample.mdDeviceSamples;

import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class mdContentProvider extends ContentProvider
{
    private static final String TAG = "mdContentProvider";

    private static final String DATABASE_NAME = "mobile_demo.db";

    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "device_samples";

    public static final String AUTHORITY = "com.clearcenter.mobile_demo.providers.mdContentProvider";

    private static final UriMatcher uri_matcher;

    private static final int DEVICE_SAMPLES = 1;

    private static HashMap<String, String> device_samples_map;

    private static class mdDatabaseHelper extends SQLiteOpenHelper
    {
        mdDatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                mdDeviceSamples.SAMPLE_ID +
                    " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                mdDeviceSamples.NICKNAME + " VARCHAR(255), " +
                mdDeviceSamples.DATA + " LONGTEXT" + ");");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from v" +
                oldVersion + " to v" + newVersion);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }

    private mdDatabaseHelper db_helper;

    public int delete(Uri uri, String where, String[] whereArgs)
    {
        int count;
        SQLiteDatabase db = db_helper.getWritableDatabase();
        switch (uri_matcher.match(uri)) {
            case DEVICE_SAMPLES:
                count = db.delete(TABLE_NAME, where, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        //getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public String getType(Uri uri)
    {
        switch (uri_matcher.match(uri)) {
            case DEVICE_SAMPLES:
                return mdDeviceSamples.CONTENT_TYPE;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues initialValues)
    {
        if (uri_matcher.match(uri) != DEVICE_SAMPLES)
            throw new IllegalArgumentException("Unknown URI " + uri);

        ContentValues values;
        if (initialValues != null)
            values = new ContentValues(initialValues);
        else
            values = new ContentValues();

        SQLiteDatabase db = db_helper.getWritableDatabase();
        long id = db.insert(TABLE_NAME, mdDeviceSamples.DATA, values);
        if (id > 0) {
            Uri sample_uri = ContentUris.withAppendedId(
                mdDeviceSamples.CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(sample_uri, null);
            Log.d(TAG,
                "Insert new record: " + Long.toString(id) +
                ", URI: " + sample_uri);
            return sample_uri;
        }

        throw new SQLException("Failed to insert row into: " + uri);
    }

    public boolean onCreate()
    {
        db_helper = new mdDatabaseHelper(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] projection,
        String selection, String[] selectionArgs, String sortOrder)
    {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (uri_matcher.match(uri)) {
            case DEVICE_SAMPLES:
                qb.setTables(TABLE_NAME);
                qb.setProjectionMap(device_samples_map);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor c = qb.query(db, projection,
            selection, selectionArgs, null, null, sortOrder);

        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    public int update(Uri uri, ContentValues values,
        String where, String[] whereArgs)
    {
        int count;
        SQLiteDatabase db = db_helper.getWritableDatabase();
        switch (uri_matcher.match(uri)) {
            case DEVICE_SAMPLES:
                count = db.update(TABLE_NAME, values, where, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    static {
        uri_matcher = new UriMatcher(UriMatcher.NO_MATCH);
        uri_matcher.addURI(AUTHORITY, TABLE_NAME, DEVICE_SAMPLES);

        device_samples_map = new HashMap<String, String>();

        device_samples_map.put(
            mdDeviceSamples.SAMPLE_ID, mdDeviceSamples.SAMPLE_ID);
        device_samples_map.put(
            mdDeviceSamples.NICKNAME, mdDeviceSamples.NICKNAME);
        device_samples_map.put(
            mdDeviceSamples.DATA, mdDeviceSamples.DATA);
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
