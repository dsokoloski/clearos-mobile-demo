// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import com.clearcenter.mobile_demo.db.mdDeviceSample.mdDeviceSamples;
import com.clearcenter.mobile_demo.mdConstants;
import com.clearcenter.mobile_demo.mdRest;
import com.clearcenter.mobile_demo.mdSSLUtil;
import com.clearcenter.mobile_demo.providers.mdContentProvider;

import org.apache.http.auth.AuthenticationException;
import org.apache.http.ParseException;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.HashMap;

public class mdSyncAdapter extends AbstractThreadedSyncAdapter
{
    private static final String TAG = "mdSyncAdapter";

    private static final boolean NOTIFY_AUTH_FAILURE = true;

    private final AccountManager account_manager;

    private final Context ctx;

    private HashMap<String, Long> last_sample;

    public mdSyncAdapter(Context context, boolean autoInitialize)
    {
        super(context, autoInitialize);

        ctx = context;
        account_manager = AccountManager.get(context);
        last_sample = new HashMap<String, Long>();

        Log.d(TAG, "SyncAdapter created...");
    }

    public void onPerformSync(
        Account account, Bundle extras, String authority,
        ContentProviderClient provider, SyncResult syncResult)
    {
        String authtoken = null;
        Log.i(TAG, "Starting sync operation...");

        try {
            mdSSLUtil.DisableSecurity();

            // Use the account manager to request the AuthToken we'll need
            // to talk to our sample server.  If we don't have an AuthToken
            // yet, this could involve a round-trip to the server to request
            // an AuthToken.
            authtoken = account_manager.blockingGetAuthToken(
                account, mdConstants.AUTHTOKEN_TYPE, NOTIFY_AUTH_FAILURE);

            final String hostname =
                account_manager.getUserData(account, "hostname");

            long _last_sample = -1;
            if (last_sample.containsKey(account.name))
                _last_sample = last_sample.get(account.name);

            // Get sync data from server...
            final String data = mdRest.GetSystemInfo(hostname, authtoken, _last_sample);

            // Something went wrong :^(
            if (TextUtils.isEmpty(data)) return;

            if (_last_sample < 0) {
                int count = provider.delete(
                    mdDeviceSamples.CONTENT_URI,
                    mdDeviceSamples.NICKNAME + " = ?",
                    new String[] { account.name });
            
                Log.d(TAG, "Reset database, purged " + count + " samples.");
            }

            try {
                JSONObject json_data = new JSONObject(data);
                if (json_data.has("time"))
                    _last_sample = Long.valueOf(json_data.getString("time"));
                Log.d(TAG, account.name + ": last sample time-stamp: " + _last_sample);
            } catch (JSONException e) {
                Log.e(TAG, "JSONException", e);
                return;
            }

            last_sample.put(account.name, _last_sample);

            ContentValues values = new ContentValues();
            values.put(mdDeviceSamples.NICKNAME, account.name);
            values.put(mdDeviceSamples.DATA, data);

            provider.insert(mdDeviceSamples.CONTENT_URI, values);

            String projection[] = new String[] {
                mdDeviceSamples.SAMPLE_ID
            };

            Cursor cursor = provider.query(
                mdDeviceSamples.CONTENT_URI,
                projection,
                mdDeviceSamples.NICKNAME + " = ?",
                new String[] { account.name },
                mdDeviceSamples.SAMPLE_ID);

            int rows = cursor.getCount();
            Log.d(TAG, "Rows: " + rows);
            if (rows <= mdConstants.MAX_SAMPLES) {
                cursor.close();
                return;
            }

            Log.d(TAG, "Samples to purge: " +
                (rows - mdConstants.MAX_SAMPLES));
            cursor.move(rows - mdConstants.MAX_SAMPLES);

            int top_id = cursor.getInt(
                cursor.getColumnIndex(mdDeviceSamples.SAMPLE_ID));
            Log.d(TAG, "Top ID: " + top_id);

            cursor.close();

            int count = provider.delete(
                mdDeviceSamples.CONTENT_URI,
                mdDeviceSamples.SAMPLE_ID + " <= ? AND " +
                mdDeviceSamples.NICKNAME + " = ?",
                new String[] { String.valueOf(top_id), account.name });

            Log.d(TAG, "Purged " + count + " samples.");

        } catch (final RemoteException e) {
            Log.e(TAG, "RemoteException", e);
            syncResult.stats.numParseExceptions++;
        } catch (final AuthenticatorException e) {
            Log.e(TAG, "AuthenticatorException", e);
            syncResult.stats.numParseExceptions++;
        } catch (final OperationCanceledException e) {
            Log.e(TAG, "OperationCanceledExcetpion", e);
        } catch (final IOException e) {
            Log.e(TAG, "IOException", e);
            syncResult.stats.numIoExceptions++;
        } catch (final ParseException e) {
            Log.e(TAG, "ParseException", e);
            syncResult.stats.numParseExceptions++;
        } catch (final AuthenticationException e) {
            Log.e(TAG, "AuthenticationException", e);
            syncResult.stats.numAuthExceptions++;
            account_manager.invalidateAuthToken(
                mdConstants.AUTHTOKEN_TYPE, authtoken);
        } catch (GeneralSecurityException e) {
            Log.e(TAG, "GeneralSecurityException", e);
        } catch (final JSONException e) {
            Log.e(TAG, "JSONException", e);
            syncResult.stats.numParseExceptions++;
        }
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
