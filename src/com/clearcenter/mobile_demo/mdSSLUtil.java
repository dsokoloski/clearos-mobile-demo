// ClearOS Mobile Demo: Example Android™ Application
// Copyright (C) 2012 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.clearcenter.mobile_demo;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class mdSSLUtil
{
    // XXX: Disable self-signed certificate and hostname verification checks
    static public void DisableSecurity() throws GeneralSecurityException
    {
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(null, new TrustManager[] {
            new X509TrustManager()
            {
                public void checkClientTrusted(
                    X509Certificate[] chain, String authType)
                    throws CertificateException { }
                public void checkServerTrusted(
                    X509Certificate[] chain, String authType)
                    throws CertificateException { }
                public X509Certificate[] getAcceptedIssuers()
                {
                    return new X509Certificate[]{};
                }
            }
        }, null);

        HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());

        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
        {
            public boolean verify(String hostname, SSLSession session)
            {
                return true;
            }
        });
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
